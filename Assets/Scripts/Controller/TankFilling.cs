﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankFilling : MonoBehaviour
{
    [SerializeField]
    private GameObject water;
    [SerializeField]
    private Vector3 position;
    private double percent;
    private double heigth;
    [SerializeField]
    private double velocity;
    private double volumen;
    private double auxiliar;


    // Start is called before the first frame update
    void Start()
    {
        position = water.transform.localPosition;


    }

    // Update is called once per frame
    void Update()
    {
        Filling();
    }
    private void Filling()
    {

        volumen = Setting.setting._volumen; //

        if (position.z < 0)
        {
            auxiliar = +volumen;
        }
        else if (position.z > 0)
        {
            auxiliar = -volumen;
        }

        Debug.Log("volumen:" + volumen);
        percent = (auxiliar * 100) / Setting.setting._containerVolume;  //regla de 3: volumen%/capacidad
        heigth = (percent * 0.0027f) / 100; //* por un factor, tambien podria estar definido por setting
        Debug.Log("percent:" + percent + "heigth:" + heigth);
        float scaley = System.Convert.ToSingle(position.z + heigth * velocity);//variable local ojo con float hay que transformas
        water.transform.localScale = new Vector3(water.transform.localScale.x, water.transform.localScale.y, scaley);//afectando la escala del objeto

    }
}