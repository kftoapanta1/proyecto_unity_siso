﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour
{
    [SerializeField]
    private RunOperation insp = new RunOperation();
    private void OnTriggerStay(Collider other)
    {
        ControlPanel(other.tag);
    }
    private void ControlPanel(string tag)
    {
        if (tag.Equals("Persona"))
        {
            float controllerUp = Input.GetAxis("Subir");
            float controllerDown = Input.GetAxis("Bajar");
            bool activeUp = false, activeDown = false;
            if (controllerUp == 1 || controllerUp == -1)
            {
                activeUp = true;
            }
            else if (controllerDown == 1 || controllerDown == -1)
            {
                activeDown = true;
            }
            if (Input.GetMouseButton(0) || activeDown)
            {
                insp._insp.value += 0.01f;

            }
            if (Input.GetMouseButton(1) || activeDown)
            {
                insp._insp.value -= 0.01f;

            }
        }

    }


}