﻿//MORALES MEMORIAS COMPARTIDAS
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;   //libreria de dependencia
using System.Runtime.InteropServices; // para que funcione Dll
using TMPro;



public class RunOperation : MonoBehaviour
{
    [SerializeField] //objeto visible en el inspector
    private Slider insp;
    public Slider _insp { set { this.insp = value; } get { return insp; } }
    private float sp;
    [SerializeField]
    private TMP_Text[] label; //arreglos para los label
    private int tf, t;
    private float volumen;
    private float error;
    private float model;
    //[SerializeField]
    //private GameObject re;
    //[SerializeField]    //no estmos rompiendo el vidrio
    //private BreakGlassController s = new BreakGlassController();

    #region ImportacionDll
    // Start is called before the first frame update
    const string dllPath = "smClient64.dll";
    // Crear la memoria: Este paso se lo hace en Matlab
    // Abrir la memoria
    [DllImport(dllPath)]
    public static extern int openMemory(String name, int typeCode);  //acciones rapidas y refactorizantes

    // Enteros set: escribir get: leer
    [DllImport(dllPath)]
    public static extern void setFloat(String memName, int position, float value);
    [DllImport(dllPath)]
    public static extern int getFloat(String memName, int position);

    private string memoriaSp = "Sp";
    private string memoriaControlador = "Controlador";
    #endregion

    void Start()
    {
        t = 1;
        for (int i = 0; i < label.Length; i++)
        {
            label[i].text = System.Convert.ToString(0);
        }
        tf = 50;
        //abrir las memorias
        openMemory(memoriaSp, 2);
        openMemory(memoriaControlador, 2);
    }

    // Update is called once per frame
    void Update()
    {
        heigResult();
    }
    private void heigResult()
    {
        float controller; //declaro el valor del controlador
        sp = System.Convert.ToSingle(insp.value * Setting.setting._containerVolume);//procedo a tomar el valor del slider  y transformar a la escala del contenedor  ya que el slider se encuentra se encuentra en el rango de 0 y 1
        Debug.Log("sp:" + sp);
        label[0].text = System.Convert.ToString(System.Math.Round(sp, 2));//asigno el valor a label y redondeando en 2 decimales  
        Setting.setting._sp = sp;//asigno los valores calculados a nuestra clase setting
                                 //Envio de memorias compartidas
        setFloat(memoriaSp, 0, sp);
        controller = getFloat(memoriaControlador, 0); //leemos memoria compartida
        Debug.Log("controller:" + controller);
        volumen = controller;//procedo a calcular el volumen mediante el controller y el modelo
        Setting.setting._volumen = volumen;//asigno los valores calculados a nuestra clase setting 
        label[1].text = System.Convert.ToString(System.Math.Round(volumen, 2));//asigno el valor a label y redondeando en 2 decimales
        error = t * Time.deltaTime * 0.5f; //posteriormente este valor sera reemplazado por el de la memoria compartida
        Setting.setting._error = error;//asigno los valores calculados a nuestra clase setting
        t += 1; //aumento la variable del t
        tf -= 2;//disminucion de la variable del tf
        label[2].text = System.Convert.ToString(System.Math.Round(error, 2));//asigno el valor a label y redondeando en 2 decimales
    }
}