﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : MonoBehaviour
{
    public static Setting setting;
    [SerializeField]
    private double containerVolume;

    public double _containerVolume { set { this.containerVolume = value; } get { return containerVolume; } }
    private double volumen;
    public double _volumen { set { this.volumen = value; } get { return volumen; } }
    private double sp;
    public double _sp { set { this.sp = value; } get { return sp; } }
    private double error;
    public double _error { set { this.error = value; } get { return error; } }

    private void Awake()
    {
        if (Setting.setting == null)
        {
            Setting.setting = this;
        }

        else
        {
            if (Setting.setting != this)
            {
                Destroy(this.gameObject);

            }
        }
        DontDestroyOnLoad(this.gameObject);
        Debug.Log(containerVolume);
    }
}